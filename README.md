## Helm Useful Commands

### Search for available Helm Repository in locally
```
helm search repo
helm search repo wordpress
```

### Search for Helm Repository in Hub
```
helm search hub nginx

# Adds Bitnami repo(s). This ONLY adds the metadata details of repository available in Bitnami
helm repo add bitnami https://charts.bitnami.com/bitnami    
helm search repo nginx
```

### Pull the Repo from Hub
```
helm pull bitnami/nginx --untar=true
```

### Install Helm Chart
```
helm install my-nginx bitnami/nginx
```

***